﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyAssets.Web.Models
{
    public class DepartmentListViewModel
    {
        public int Index { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string DepartmentHead { get; set; }

        public DateTime CreatedOn  { get; set; }

    }
}
