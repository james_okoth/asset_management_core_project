﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyAssets.Web.Models
{
    public class NewAssetViewModel
    {
        public int AssetId { get; set; }
        [Required(ErrorMessage="Add Title")]
        public string Title { get; set; }
        [Required(ErrorMessage ="Add description")]
        public string Description { get; set; }
        
        public int DepartmentId { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
