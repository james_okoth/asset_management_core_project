﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyAssets.Web.Models
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage="Please provide Name")]
        public string FullName { get; set; }
        [Required(ErrorMessage ="Select Gender")]
        public string Gender { get; set; }
        [Required(ErrorMessage ="Enter your email")]
        public string Email { get; set; }
        public string ImageUri { get; set; }
        public DateTime AddedOn { get; set; }
        public int DepartmentId { get; set; }
    }
}

