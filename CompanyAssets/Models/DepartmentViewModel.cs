﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyAssets.Web.Models
{
    public class DepartmentViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please Add Title")]
        public string Title { get; set; }
        [Required(ErrorMessage ="Add Description")]
        public string Description { get; set; }
        public string DepartmentHead { get; set; }
        public int EmployeeId { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
