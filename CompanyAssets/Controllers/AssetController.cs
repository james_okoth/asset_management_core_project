﻿using CompanyAssets.Core.Models;
using CompanyAssets.Core.Repositories;
using CompanyAssets.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyAssets.Web.Controllers
{
    public class AssetController : Controller
    {
        private readonly IGlobalService<Asset> _assetService;
        public AssetController(IGlobalService<Asset> assetService)
        {
            _assetService = assetService;
        }
        [HttpGet]
        public IActionResult AddAsset()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddAsset(NewAssetViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var _errors = new List<string>();
                foreach (var _values in ModelState.Values)
                {
                    foreach (var _error in _values.Errors)
                    {
                        _errors.Add(_error.ErrorMessage);
                    }
                }
                return Ok(new { check = false, message = string.Join(", ", _errors) });
            }
            try
            {

                var _asset = _assetService.Add(new Asset
                {
                    CreatedOn = DateTime.UtcNow.Date,
                    Title = model.Title,
                    Description = model.Description,
                    IsDeleted = false,
                    DepartmentId=model.DepartmentId

                });

                return RedirectToAction("LoadAll");
            }
            catch (Exception ex)
            {
                return Ok(new { check = false, message = "Failed, please fill in all the required sections" });
            }
        }

        public IActionResult LoadAll()
        {
            var _assets = _assetService.Search(x => true).OrderByDescending(x => x.CreatedOn);

            var _assetList = new List<NewAssetViewModel>();

            foreach (var _asset in _assets)
            {

                _assetList.Add(new NewAssetViewModel
                {
                    AssetId = _asset.AssetId,
                    Title = _asset.Title,
                    Description = _asset.Description,
                   
                    CreatedOn = _asset.CreatedOn

                });
            }

            return View(_assetList);
        }
        public IActionResult EditAsset()
        {
            return View();
        }
        public IActionResult DeleteAsset()
        {
            return View();
        }
        public IActionResult ViewAsset(int Id)
        {
            var _asset = _assetService.Get(Id);

            return View(_asset);
        }
        public IActionResult DeleteAsset(int id)
        {
            var _asset = _assetService.Get(id);


            try
            {

                _assetService.Delete(_asset);
                return RedirectToAction("LoadAll");

                // return Ok(new { check = true, message = "employee has been deleted successfully" });
            }
            catch (Exception ex)
            {


                return Ok(new { check = false, message = "Failed, try again" });
            }

        }
        public IActionResult EditAsset(int Id)
        {
            var _asset = _assetService.Get(Id);
            try
            {
                _assetService.Update(_asset);
                return RedirectToAction("LoadAll");
            }
            catch (Exception ex)
            {


                return Ok(new { check = false, message = "Failed, try again" });
            }
        }

    }
}
