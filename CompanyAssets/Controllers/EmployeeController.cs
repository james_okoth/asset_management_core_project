﻿using CompanyAssets.Core.Models;
using CompanyAssets.Core.Repositories;
using CompanyAssets.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyAssets.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IGlobalService<Employee> _employeeService;
        public EmployeeController(IGlobalService<Employee> employeeService)
        {
            _employeeService = employeeService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]

        public IActionResult AddEmployee()
        {
            return View();
        }


        [HttpPost]
        public IActionResult AddEmployee(EmployeeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var _errors = new List<string>();
                foreach (var _values in ModelState.Values)
                {
                    foreach (var _error in _values.Errors)
                    {
                        _errors.Add(_error.ErrorMessage);
                    }
                }
                return Ok(new { check = false, message = string.Join(", ", _errors) });
            }
            try
            {

                var _depart = _employeeService.Add(new Employee
                {
                    AddedOn = DateTime.UtcNow.Date,
                    FullName = model.FullName,
                    Email = model.Email,
                    Gender = model.Gender,
                    DepartmentId = model.DepartmentId,
                    ImageUri = model.ImageUri

                });

                return RedirectToAction("LoadAll");
                //return Ok(new { check = true, message = "You have Successfully Registered.." });






            }
            catch (Exception ex)
            {
                return Ok(new { check = false, message = "Failed, please fill in all the required sections" });
            }

        }
        public ActionResult LoadAll()
        {
            var _employees = _employeeService.Search(x => true).OrderByDescending(x => x.AddedOn);

            var _empList = new List<EmployeeViewModel>();

            foreach (var _emp in _employees)
            {

                _empList.Add(new EmployeeViewModel
                {
                    Id = _emp.Id,
                    FullName = _emp.FullName,
                    Gender = _emp.Gender,
                    Email = _emp.Email,
                    ImageUri = _emp.ImageUri,
                    DepartmentId = _emp.DepartmentId

                });
            }

            return View(_empList);

        }

        public IActionResult GetEmployees()
        {
            var _emps = _employeeService.Search(x => true);
            return Ok(new { d = _emps });
        }

        public IActionResult DeleteEmployee(int id)
        {
            var _emp = _employeeService.Get(id);


            try
            {

                _employeeService.Delete(_emp);
                return RedirectToAction("LoadAll");

               // return Ok(new { check = true, message = "employee has been deleted successfully" });
            }
            catch (Exception ex)
            {


                return Ok(new { check = false, message = "Failed, try again" });
            }
            


        }
        public IActionResult ViewEmployee(int Id)
        {
            var _employee = _employeeService.Get(Id);

            return View(_employee);
        }

        public IActionResult EditEmployee(int Id)
        {
            var _emp = _employeeService.Get(Id);
            try
            {
                _employeeService.Update(_emp);
                return RedirectToAction("LoadAll");
            }
            catch (Exception ex)
            {


                return Ok(new { check = false, message = "Failed, try again" });
            }
        }
    }
}