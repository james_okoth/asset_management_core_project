﻿using CompanyAssets.Core.Models;
using CompanyAssets.Core.Repositories;
using CompanyAssets.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyAssets.Web.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly IGlobalService<Department> _departService;
        public DepartmentController(IGlobalService<Department> departService)
        {
            _departService = departService;  
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]

        public IActionResult AddDepartment()
        {
            return View();
        }


        [HttpPost]
        public IActionResult AddDepartment(DepartmentViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var _errors = new List<string>();
                foreach (var _values in ModelState.Values)
                {
                    foreach (var _error in _values.Errors)
                    {
                        _errors.Add(_error.ErrorMessage);
                    }
                }
                return Ok(new { check = false, message = string.Join(", ", _errors) });
            }
            try
            {

                var _depart = _departService.Add(new Department
                {
                    CreatedOn = DateTime.UtcNow.Date,
                     Title=model.Title,
                     Description=model.Description,
                      DepartmentHead=model.DepartmentHead

                });

                return RedirectToAction("LoadAll");
                //return Ok(new { check = true, message = "You have Successfully Registered.." });
                





            }
            catch (Exception ex)
            {
                return Ok(new { check = false, message = "Failed, please fill in all the required sections" });
            }

        }
        public ActionResult LoadAll()
        {
            var _departs = _departService.Search(x => true).OrderByDescending(x => x.CreatedOn);

            var _departList = new List<DepartmentViewModel>();

            foreach (var _depart in _departs)
            {
                
                _departList.Add(new DepartmentViewModel
                {
                    Id = _depart.Id,
                    Title = _depart.Title,
                    Description = _depart.Description,
                    DepartmentHead = _depart.DepartmentHead,
                    CreatedOn = _depart.CreatedOn
                    
                });
            }

            return View(_departList);

        }

        public IActionResult DeleteDepartment(int id)
        {
            var _depart = _departService.Get(id);


            try
            {

                _departService.Delete(_depart);
                return RedirectToAction("LoadAll");

                // return Ok(new { check = true, message = "employee has been deleted successfully" });
            }
            catch (Exception ex)
            {


                return Ok(new { check = false, message = "Failed, try again" });
            }



        }


        public IActionResult Getdeparts()
        {
            var _departs = _departService.Search(x => true);
            return Ok(new { d = _departs });
        }
        public IActionResult ViewDepartment(int Id)
        {
            var _depart = _departService.Get(Id);

            return View(_depart);
        }
        public IActionResult EditDepartment(int Id)
        {
            var _asset = _departService.Get(Id);
            try
            {
                _departService.Update(_asset);
                return RedirectToAction("LoadAll");
            }
            catch (Exception ex)
            {


                return Ok(new { check = false, message = "Failed, try again" });
            }
        }
    }
}
