﻿using CompanyAssets.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyAssets.Core.Context
{
    public class AssetContext:DbContext
    {
        public AssetContext(DbContextOptions<AssetContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Asset> Assets { get; set; }
    }
}
