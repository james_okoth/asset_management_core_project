﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyAssets.Core.Context
{
    public class DbContextFactory
    {
        readonly IConfiguration configuration;
        public DbContextFactory(IConfiguration appConfiguration)
        {
            configuration = appConfiguration;
        }
        public AssetContext GetContext()
        {
            string dbConfigName = configuration["RunTimeSettings:UseConnectionString"];

            var builder = new DbContextOptionsBuilder<AssetContext>()
                .UseSqlServer(configuration.GetConnectionString(dbConfigName), configAction =>
                {
                    configAction.EnableRetryOnFailure(
                        maxRetryCount: 10,
                        maxRetryDelay: TimeSpan.FromSeconds(30),
                        errorNumbersToAdd: null);
                });

            var _context = new AssetContext(builder.Options);

            var dbConnextion = _context.Database.GetDbConnection();

            if (dbConnextion.State == System.Data.ConnectionState.Closed)
                dbConnextion.Open();

            return _context;
        }
    }
}
