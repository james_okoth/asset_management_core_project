﻿using CompanyAssets.Core.DataManager;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace CompanyAssets.Core.Repositories
{
    public class GlobalService<T> : IGlobalService<T> where T : class
    {
        private readonly IDataManager<T> _repo;
        public GlobalService(IDataManager<T> repo)
        {
            _repo = repo;
        }
        public T Add(T entity)
        {
            return _repo.Add(entity);
        }

        public void Delete(T entity)
        {
            _repo.Delete(entity);
        }

        public T Get(int Id)
        {
            return _repo.Get(Id);
        }

        public IEnumerable<T> Search(Expression<Func<T, bool>> expression)
        {
            return _repo.Search(expression);
        }

        public void Update(T entity)
        {
            _repo.Update(entity);
        }
    }
}
