﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyAssets.Core.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string ImageUri { get; set; }
        public DateTime AddedOn { get; set; }
        public int DepartmentId { get; set; }
    }
}
