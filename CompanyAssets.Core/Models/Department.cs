﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyAssets.Core.Models
{
    public class Department
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DepartmentHead { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
