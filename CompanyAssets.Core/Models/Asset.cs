﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyAssets.Core.Models
{
    public class Asset
    {
        public int AssetId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int DepartmentId { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }

    }
}
