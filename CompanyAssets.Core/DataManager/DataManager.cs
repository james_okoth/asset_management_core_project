﻿using CompanyAssets.Core.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CompanyAssets.Core.DataManager
{
    public class DataManager<T> : IDataManager<T> where T : class
    {
        private readonly DbContextFactory _factory;
        public DataManager(DbContextFactory factory)
        {
            _factory = factory;
        }
        public T Add(T entity)
        {
            using var context = _factory.GetContext();
            context.Add(entity);
            context.SaveChanges();
            return entity;
        }

        public void Delete(T entity)
        {
            using var context = _factory.GetContext();
            context.Remove(entity);
            context.SaveChanges();
        }

        public T Get(int Id)
        {
            using var context = _factory.GetContext();
            var TEntity = context.Set<T>().Find(Id);
            return TEntity;
        }

        public IEnumerable<T> Search(Expression<Func<T, bool>> expression)
        {
            using var context = _factory.GetContext();
            var TEntities = context.Set<T>().Where(expression).ToList();
            return TEntities;
        }

        public void Update(T entity)
        {
            using var context = _factory.GetContext();
            context.Set<T>().Update(entity);
            context.SaveChanges();
        }
    }
}
