﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace CompanyAssets.Core.DataManager
{
    public interface IDataManager<T> where T:class
    {
        T Add(T entity);
        void Update(T entity);
        T Get(int Id);
        void Delete(T entity);
        IEnumerable<T> Search(Expression<Func<T, bool>> expression);

    }
}
